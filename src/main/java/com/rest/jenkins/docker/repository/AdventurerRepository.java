package com.rest.jenkins.docker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rest.jenkins.docker.model.Adventurer;

@Repository
public interface AdventurerRepository extends JpaRepository<Adventurer, Long>{

}

