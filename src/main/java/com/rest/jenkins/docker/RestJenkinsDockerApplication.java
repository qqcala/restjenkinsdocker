package com.rest.jenkins.docker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestJenkinsDockerApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestJenkinsDockerApplication.class, args);
	}

}
