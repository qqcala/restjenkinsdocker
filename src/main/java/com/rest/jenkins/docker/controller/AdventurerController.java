package com.rest.jenkins.docker.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.rest.jenkins.docker.model.Adventurer;
import com.rest.jenkins.docker.repository.AdventurerRepository;

@RestController
@RequestMapping("/adventurer")
public class AdventurerController {

	@Autowired
	AdventurerRepository adventurerRepository;
	
	@GetMapping("/list")
	public List<Adventurer> listCharacters() {
		return this.adventurerRepository.findAll();
	}
	
	@PostMapping("/create")
	public Adventurer createCharacter(@Valid @RequestBody Adventurer adventurer) {
		return adventurerRepository.save(adventurer);
	}
	
}
