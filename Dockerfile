FROM openjdk:8
EXPOSE 9092
ADD target/rest-jenkins-docker.jar rest-jenkins-docker.jar
ENTRYPOINT ["java", "-jar", "/rest-jenkins-docker.jar"]